<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Radars</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"/>
</head>
<body>
    <div id="mapid" style="width: 100%; height: 74em;"></div>
</body>
</html>



<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>

<script type="text/javascript">
    var map = L.map('mapid').setView([48.117266, -1.6777926], 10);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);


    const downloadCsv = async () => {
        const target = `./radars.csv`;

        const res = await fetch(target, {
            method: "get",
            headers: { "content-type": "text/csv;charset=UTF-8" },
        });
        const data = await res.text();
        const donnee = data.split(/\r?\n/);
        donnee.forEach((data) =>
        {
            const splited = data.split(",")
            var fes = L.marker([splited[3], splited[4]]).addTo(map);
        })
    };
    downloadCsv();
</script>